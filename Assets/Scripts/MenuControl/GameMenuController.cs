﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameMenuController : MonoBehaviour {

    public static GameMenuController Instance;

    public GameObject PausePanel;
    public GameObject ControlPanel;
    public GameObject WinPanel;
    public GameObject LosePanel;

    public enum enMenuStates
    {
        Pause,
        Normal,
        Victory,
        Loose
    }

    public enMenuStates MenuStates = enMenuStates.Normal;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        Time.timeScale = 1;
    }

    private void Update()
    {
        // todo - REMOVE IT!!
        if (Input.GetKeyDown(KeyCode.L))
        {
            OnLose();
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            OnVictory();
        }
    }

    public void MenuLoader()
    {
        CustomLevelLoader.LoadLevel("menu");
    }

    public void QuitApp()
    {
        CustomLevelLoader.QuitGame();
    }

    public void RestartLevel()
    {
        CustomLevelLoader.ReloadCurrentScene();
    }

    public void OnPauseButtonClick()
    {
        // AdsController.ShowAds();
        AdsController.ShowRewardedVideo();
        MenuStates = enMenuStates.Pause;
        Refresh();
        Time.timeScale = 0;
    }

    public void OnResumeButtonClick()
    {
        MenuStates = enMenuStates.Normal;
        Refresh();
        Time.timeScale = 1;
    }

    public void OnVictory()
    {
        MenuStates = enMenuStates.Victory;
        Refresh();
        Time.timeScale = 0;
    }

    public void OnLose()
    {
        MenuStates = enMenuStates.Loose;
        Refresh();
        Time.timeScale = 0;
    }

    public void OnNextLevelButtonClick()
    {
        LevelSelect.CurLevel++;
        CustomLevelLoader.LoadLevel("game");
    }

    private void Refresh()
    {
        switch (MenuStates)
        {
            case enMenuStates.Pause:
                PausePanel.SetActive(true);
                ControlPanel.SetActive(false);
                LosePanel.SetActive(false);
                WinPanel.SetActive(false);
                break;
            case enMenuStates.Normal:
                PausePanel.SetActive(false);
                ControlPanel.SetActive(true);
                LosePanel.SetActive(false);
                WinPanel.SetActive(false);
                break;
            case enMenuStates.Victory:
                PausePanel.SetActive(false);
                ControlPanel.SetActive(false);
                LosePanel.SetActive(false);
                WinPanel.SetActive(true);
                break;
            case enMenuStates.Loose:
                PausePanel.SetActive(false);
                ControlPanel.SetActive(false);
                LosePanel.SetActive(true);
                WinPanel.SetActive(false);
                break;
            default:
                Debug.Log("switch-case bug");
                break;
        }
    }
}
