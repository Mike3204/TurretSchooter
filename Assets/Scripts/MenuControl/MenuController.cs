﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour {

    public GameObject NormalPanel;
    public GameObject SettingsPanel;
    public GameObject LevelSelectPanel;
    public GameObject ShopPanel;
    public GameObject RealMoneyShopPanel;
    public GameObject WeaponSelectPanel;

    private enum enMenuType
    {
        Normal,
        Settings,
        LevelSelect,
        Shop,
        WeaponSelect,
        RealMoneyShop
    }

    private enMenuType menuType = enMenuType.Normal;

    private void Start()
    {
        Refresh();
    }

    /*
#if UNITY_EDITOR
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Delete))
        {
            PlayerPrefsHelper.DeleteAll();
        }
    }
#endif
*/

    public void OnPlayClick()
    {
        menuType = enMenuType.LevelSelect;
        Refresh();
    }

    public void OnSettingsButtonClick()
    {
        menuType = enMenuType.Settings;
        Refresh();
    }

    public void OnBackButtonClick()
    {
        menuType = enMenuType.Normal;
        Refresh();
    }

    public void OnShopButtonClick()
    {
        menuType = enMenuType.Shop;
        Refresh();
    }

    public void OnRealMoneyShopButtonClick()
    {
        menuType = enMenuType.RealMoneyShop;
        Refresh();
    }

    public void OnLevelSelect()
    {
        menuType = enMenuType.WeaponSelect;
        Refresh();
    }

    private void Refresh()
    {
        switch (menuType)
        {
            case enMenuType.Normal:
                SettingsPanel.SetActive(false);
                NormalPanel.SetActive(true);
                LevelSelectPanel.SetActive(false);
                ShopPanel.SetActive(false);
                WeaponSelectPanel.SetActive(false);
                RealMoneyShopPanel.SetActive(false);
                break;
            case enMenuType.Settings:
                SettingsPanel.SetActive(true);
                NormalPanel.SetActive(false);
                LevelSelectPanel.SetActive(false);
                ShopPanel.SetActive(false);
                WeaponSelectPanel.SetActive(false);
                RealMoneyShopPanel.SetActive(false);
                break;
            case enMenuType.LevelSelect:
                SettingsPanel.SetActive(false);
                NormalPanel.SetActive(false);
                LevelSelectPanel.SetActive(true);
                ShopPanel.SetActive(false);
                WeaponSelectPanel.SetActive(false);
                RealMoneyShopPanel.SetActive(false);
                break;
            case enMenuType.Shop:
                SettingsPanel.SetActive(false);
                NormalPanel.SetActive(false);
                LevelSelectPanel.SetActive(false);
                ShopPanel.SetActive(true);
                WeaponSelectPanel.SetActive(false);
                RealMoneyShopPanel.SetActive(false);
                break;
            case enMenuType.WeaponSelect:
                SettingsPanel.SetActive(false);
                NormalPanel.SetActive(false);
                LevelSelectPanel.SetActive(false);
                ShopPanel.SetActive(false);
                WeaponSelectPanel.SetActive(true);
                RealMoneyShopPanel.SetActive(false);
                break;
            case enMenuType.RealMoneyShop:
                SettingsPanel.SetActive(false);
                NormalPanel.SetActive(false);
                LevelSelectPanel.SetActive(false);
                ShopPanel.SetActive(false);
                WeaponSelectPanel.SetActive(false);
                RealMoneyShopPanel.SetActive(true);
                break;
            default:
                Debug.Log("MenuController error");
                break;
        }
    }
}
