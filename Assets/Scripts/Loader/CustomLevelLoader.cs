﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class CustomLevelLoader {

    static string selectedScene = "";
    
    /// <summary>
    /// Loads the level
    /// </summary>
    /// <param name="sceneName">Scene name</param>
    public static void LoadLevel(string sceneName)
    {
        selectedScene = sceneName;
        SceneManager.LoadScene("loading");
    }

    /// <summary>
    /// Using for scene Loading
    /// </summary>
    public static void LoadSelectedLevel()
    {
        if (string.IsNullOrEmpty(selectedScene))
        {
            SceneManager.LoadScene("menu");
        }
        else
        {
            SceneManager.LoadScene(selectedScene);
        }
    }

    public static void ReloadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public static void QuitGame()
    {
        Application.Quit();
    }
}
