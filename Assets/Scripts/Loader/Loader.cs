﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Loader : MonoBehaviour {

    public Image FillImage;
    public Text LoadText;
    public Text fakeLoadText;
    public string[] FakeTitles;

    private float loadTime = 1.5f;
    private float curLoadTime;
    private float curFakeTitle;
    private float timeForTitle;
    private int titleCounter;

    // Use this for initialization
    private void Awake()
    {
        timeForTitle = loadTime / FakeTitles.Length;
        curFakeTitle = timeForTitle;
        StartCoroutine(FakeLoad());
    }

    private void OnDestroy()
    {
        StopCoroutine(FakeLoad());
    }

    private IEnumerator FakeLoad()
    {
        do
        {
            yield return new WaitForEndOfFrame();
            curLoadTime += Time.unscaledDeltaTime;
            FillImage.fillAmount = curLoadTime / loadTime;
            LoadText.text = Mathf.RoundToInt((curLoadTime / loadTime) * 100).ToString() + "%";

            if (curLoadTime <= curFakeTitle)
            {
                fakeLoadText.text = FakeTitles[titleCounter];
            }
            else
            {
                curFakeTitle += timeForTitle;
                titleCounter++;
            }
        }
        while (FillImage.fillAmount != 1);

        Load();
    }

    private void Load()
    {
        CustomLevelLoader.LoadSelectedLevel();
    }
}
