﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof (Rigidbody))]
public class BoxEnemy : StaticEnemies {

    private Rigidbody boxRb;

    public override void Damage(int damage)
    {
        base.Damage(damage);
        if(boxRb == null)
        {
            boxRb = GetComponent<Rigidbody>();
        }

        boxRb.AddForce(Vector3.forward * 300);
    }

    private void OnDestroy()
    {
        Schooting.BulletCounter += 10;
    }
}
