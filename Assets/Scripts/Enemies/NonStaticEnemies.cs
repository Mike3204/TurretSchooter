﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NonStaticEnemies : Enemies { // наследование класса

    public Transform Path;
    public float distanceToNextNode;
    private Transform var;
    protected NavMeshAgent enemyAgent;
    private List<Transform> PathNodes = new List<Transform>();
    private Transform enemyTarget;
    private float distance;
    private int currentNode = 0;

    // Use this for initialization
    public virtual void Start()
    {
        for (int i = 0; i < Path.childCount; i++)
        {
            PathNodes.Add(Path.GetChild(i));
        }
        enemyAgent = GetComponent<NavMeshAgent>();
        enemyTarget = PathNodes[0];
    }

    // Update is called once per frame
    public virtual void Update()
    {
        enemyAgent.SetDestination(enemyTarget.position);
        distance = Vector3.Distance(transform.position, enemyTarget.position);
        if (distance <= distanceToNextNode)
        {
            if (currentNode == Path.childCount - 1)
            {
                currentNode = 0;
            }
            else
            {
                currentNode++;
            }
            enemyTarget = PathNodes[currentNode];
        }
    }
}
