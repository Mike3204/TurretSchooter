﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour {

    public int health;

	public virtual void Damage (int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            if (TutorialController.OnNextStage != null)
            {
                TutorialController.OnNextStage(3);
            }

            Destroy(gameObject);
            LevelController.Instance.KillEnemy();
        }
    }
}
