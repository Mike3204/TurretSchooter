﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour {

    private Rigidbody rigidbody;
    private RaycastHit hit;

    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if(Physics.Raycast(transform.position, Vector3.down, out hit, 5f))
        {
            rigidbody.AddForce(Vector3.up  * 2f, ForceMode.Impulse);
            rigidbody.AddTorque(Vector3.up * 5, ForceMode.Acceleration);
            rigidbody.AddTorque(Vector3.left * 5, ForceMode.Acceleration);
        }
	}
}
