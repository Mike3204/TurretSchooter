﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterRotation : MonoBehaviour {

    public float speed = 5f;
    private Vector3 newAngle;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        newAngle.z += speed;
        transform.localEulerAngles = newAngle;
	}
}
